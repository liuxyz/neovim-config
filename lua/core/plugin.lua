return require("packer").startup({
	function(use)
		-- Packer can manage itself
		use("wbthomason/packer.nvim")

		-- ui
		-- use({
		-- 	"folke/noice.nvim",
		-- 	config = function()
		-- 		require("noice").setup({
		-- 			-- add any options here
		-- 		})
		-- 	end,
		-- 	requires = {
		-- 		-- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
		-- 		"MunifTanjim/nui.nvim",
		-- 		-- OPTIONAL:
		-- 		--   `nvim-notify` is only needed, if you want to use the notification view.
		-- 		--   If not available, we use `mini` as the fallback
		-- 		"rcarriga/nvim-notify",
		-- 	},
		-- })
		use({
			"max397574/better-escape.nvim",
			config = function()
				require("better_escape").setup({})
			end,
		})
		use({
			"nvim-tree/nvim-tree.lua",
			requires = {
				"kyazdani42/nvim-web-devicons", -- optional, for file icons
			},
		})

		use({
			"https://gitlab.com/yorickpeterse/nvim-window",
			as = "nvim-window",
		})

		use({
			"nvim-telescope/telescope.nvim",
			tag = "0.1.0",
			requires = {
				{ "nvim-lua/plenary.nvim" },
				{ "BurntSushi/ripgrep" },
			},
		})
		use({ "FeiyouG/command_center.nvim" })

		use("folke/which-key.nvim")

		use({
			"romgrk/barbar.nvim",
			requires = { "kyazdani42/nvim-web-devicons" },
		})
		use({
			"nvim-lualine/lualine.nvim",
			requires = { "kyazdani42/nvim-web-devicons", opt = true },
		})

		use({
			"lewis6991/gitsigns.nvim",
			-- tag = 'release' -- To use the latest release
		})
		use({ "sindrets/diffview.nvim", requires = "nvim-lua/plenary.nvim" })

		use("Pocco81/auto-save.nvim")
		use("djoshea/vim-autoread")

		use({ "ggandor/leap.nvim" })

		use({ "echasnovski/mini.nvim", branch = "stable" })
		-- chat gpt
		-- use({
		-- 	"dpayne/CodeGPT.nvim",
		-- 	requires = {
		-- 		{ "MunifTanjim/nui.nvim" },
		-- 		{ "nvim-lua/plenary.nvim" },
		-- 	},
		-- })
		-- tabnine
		use({ "codota/tabnine-nvim", run = "./dl_binaries.sh" })

		use({
			"folke/todo-comments.nvim",
			requires = "nvim-lua/plenary.nvim",
		})

		use({
			"petertriho/nvim-scrollbar",
			config = function()
				require("scrollbar").setup()
			end,
		})

		-- lsp相关
		use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" })
		use({ "nvim-treesitter/nvim-treesitter-context" })

		use("lukas-reineke/indent-blankline.nvim")

		use({ "williamboman/mason.nvim" })
		use("neovim/nvim-lspconfig")
		use({
			"glepnir/lspsaga.nvim",
			branch = "main",
		})
		-- use("ThePrimeagen/refactoring.nvim")
		use({
			"jose-elias-alvarez/null-ls.nvim",
			requires = {
				{ "nvim-lua/plenary.nvim" },
			},
		})

		-- lsp 进度条
		use({"j-hui/fidget.nvim", tag = "legacy"})

		use("ray-x/go.nvim")
		use("ray-x/guihua.lua")

		use("simrat39/symbols-outline.nvim")

		-- 代码补全

		-- 补全引擎

		-- snippet 引擎
		-- use("hrsh7th/vim-vsnip")
		use({
			"L3MON4D3/LuaSnip",
			-- follow latest release.
			tag = "v1.2.1",
			-- install jsregexp (optional!:).
			run = "make install_jsregexp",
		})
		use("hrsh7th/nvim-cmp")
		use({ "saadparwaiz1/cmp_luasnip" })

		use({ "onsails/lspkind.nvim", branch = "master" })
		-- 补全源
		use("hrsh7th/cmp-nvim-lsp") -- { name = nvim_lsp }
		use("hrsh7th/cmp-buffer") -- { name = 'buffer' },
		use("hrsh7th/cmp-path") -- { name = 'path' }
		use("hrsh7th/cmp-cmdline") -- { name = 'cmdline' }
		use("hrsh7th/cmp-nvim-lsp-signature-help")

		-- 常见编程语言代码段
		use("rafamadriz/friendly-snippets")

		use({
			"windwp/nvim-autopairs",
			-- config = function() require("nvim-autopairs").setup {} end
		})
		use({
			"numToStr/Comment.nvim",
			config = function()
				require("Comment").setup()
			end,
		})

		use({
			"folke/trouble.nvim",
			requires = "nvim-tree/nvim-web-devicons",
			config = function() end,
		})

		use({ "catppuccin/nvim", as = "catppuccin" })

		-- 显示光标跳转
		-- 搜索时候光标很奇怪
		use("danilamihailov/beacon.nvim")

		-- debug 相关
		use("mfussenegger/nvim-dap")
		use("rcarriga/nvim-dap-ui")
		use("theHamsta/nvim-dap-virtual-text")
		use("nvim-telescope/telescope-dap.nvim")

		-- terminal
		use({ "akinsho/toggleterm.nvim", tag = "*", config = function() end })

		-- useless

		use("eandrju/cellular-automaton.nvim")
	end,
	config = {
		display = {
			open_fn = require("packer.util").float,
		},
	},
})
