local options = {
	-- filters = {
	-- 	dotfiles = false,
	-- 	exclude = { vim.fn.stdpath "config" .. "/lua/custom" },
	-- },
	-- disable_netrw = true,
	-- hijack_netrw = true,
	-- open_on_setup = false,
	-- ignore_ft_on_setup = { "alpha" },
	hijack_cursor = true,
	-- hijack_unnamed_buffer_when_opening = false,
	-- update_cwd = true,
	update_focused_file = {
		enable = true,
		update_cwd = false,
	},
	view = {
		adaptive_size = true,
		side = "left",
		width = 25,
	},
	-- git = {
	-- 	enable = true,
	-- 	ignore = true,
	-- },
	filesystem_watchers = {
		enable = true,
	},
	actions = {
		open_file = {
			resize_window = true,
		},
	},
}
require("nvim-tree").setup(options)
