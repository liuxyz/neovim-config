local command_center = require("command_center")
local noremap = { noremap = true }
local silent_noremap = { noremap = true, silent = true }

command_center.add({
	{
		-- You can specify multiple keys for the same cmd ...
		desc = "Show document symbols",
		cmd = "<CMD>Telescope lsp_document_symbols<CR>",
	},
})

require("telescope").load_extension("command_center")
local custom_command = {}

function custom_command.add_command(M)
	local commands = {}
	for _, modMap in pairs(M) do
		for mod, keyMap in pairs(modMap) do
			if mod == "o" then
				for _, it in pairs(keyMap) do
					table.insert(commands, {
						desc = it[2],
						cmd = it[1],
					})
				end
      else
        for key, it in pairs(keyMap) do
					table.insert(commands, {
						desc = it[2],
						cmd = it[1],
						keys = { mod, key, silent_noremap },
					})
        end
      end
		end
	end

	command_center.add(commands)
end

return custom_command
