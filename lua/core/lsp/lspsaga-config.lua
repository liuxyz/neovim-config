local ok, lspsaga = pcall(require, "lspsaga")
if ok then
	lspsaga.setup({
		symbol_in_winbar = {
			enable = false,
		},
		ui = {
			-- This option only works in Neovim 0.9
			title = true,
			-- Border type can be single, double, rounded, solid, shadow.
			border = "single",
			winblend = 0,
			expand = "",
			collapse = "",
			code_action = "💡",
			incoming = " ",
			outgoing = " ",
			hover = " ",
			kind = {},
		},
		lightbulb = {
			enable = false,
			enable_in_insert = false,
			sign = true,
			sign_priority = 40,
			virtual_text = true,
		},
		code_action = {
			num_shortcut = true,
			show_server_name = false,
			extend_gitsigns = true,
			keys = {
				-- string | table type
				quit = { "<ESC>", "q" },
				exec = "<CR>",
			},
		},
		rename = {
			quit = "<ESC>",
			exec = "<CR>",
			mark = "x",
			confirm = "<CR>",
			in_select = true,
		},
	})
end
