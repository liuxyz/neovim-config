local ok, go = pcall(require, "go")
if ok then
	go.setup({
		-- other setups ....
		lsp_cfg = {
			on_attach = function(client, bufnr)
				client.server_capabilities.document_formatting = false
				client.server_capabilities.document_range_formatting = false

				-- Set autocommands conditional on server_capabilities
				vim.cmd([[
	        autocmd CursorHold * set updatetime=800
          augroup lsp_document_highlight
	      	  autocmd! * <buffer>
	      	  autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
	      	  autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
          augroup END
        ]])
			end,
		},
		lsp_diag_hdlr = true, -- hook lsp diag handler
		lsp_diag_underline = false,
		-- virtual text setup
		lsp_diag_virtual_text = { space = 0, prefix = "" },
		lsp_diag_signs = true,
		lsp_diag_update_in_insert = false,
		lsp_document_formatting = false,
		-- set to true: use gopls to format
		-- false if you want to use other formatter tool(e.g. efm, nulls)
		lsp_inlay_hints = {
			enable = true,
			-- Only show inlay hints for the current line
			only_current_line = false,
			-- Event which triggers a refersh of the inlay hints.
			-- You can make this "CursorMoved" or "CursorMoved,CursorMovedI" but
			-- not that this may cause higher CPU usage.
			-- This option is only respected when only_current_line and
			-- autoSetHints both are true.
			only_current_line_autocmd = "CursorHold",
			-- whether to show variable name before type hints with the inlay hints or not
			-- default: false
			show_variable_name = true,
			-- prefix for parameter hints
			parameter_hints_prefix = " ",
			show_parameter_hints = true,
			-- prefix for all the other hints (type, chaining)
			other_hints_prefix = "=> ",
			-- whether to align to the lenght of the longest line in the file
			max_len_align = false,
			-- padding from the left if max_len_align is true
			max_len_align_padding = 1,
			-- whether to align to the extreme right or not
			right_align = false,
			-- padding from the right if right_align is true
			right_align_padding = 6,
			-- The color of the hints
			highlight = "Comment",
		},
	})
end
