local null_ls = require("null-ls")
local my_go_action = {
	filetypes = { "go" },
	name = "my custom go action",
	method = null_ls.methods.CODE_ACTION,
	generator = {
		fn = function(params)
			local actions = {
				{
					title = "nothing",
					action = function()
            -- TODO: 实现补全代码操作
						print("nothing")
					end,
				},
			}
			return actions
		end,
	},
}

null_ls.register(my_go_action)

