pcall(
	vim.cmd,
	[[
		if exists("g:neovide")
			let g:neovide_transparency=1
			let g:neovide_scroll_animation_length = 0.8
			let guifont = "Spleen 32x64"
		endif
	]]
)

vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.wo.number = true
vim.wo.relativenumber = true

-- jkhl 移动时光标周围保留8行
vim.o.scrolloff = 8
vim.o.sidescrolloff = 8

-- 禁止创建备份文件
vim.o.backup = false
vim.o.writebackup = false
vim.o.swapfile = false

vim.o.mouse = ""
vim.o.tabstop = 2

local opt = vim.opt

opt.cursorline = true
opt.clipboard = "unnamedplus"


-- Indenting
opt.expandtab = true
opt.shiftwidth = 2
opt.smartindent = true
opt.tabstop = 2
opt.softtabstop = 2


opt.fillchars = { eob = " " }
opt.ignorecase = true
opt.smartcase = true
opt.mouse = "a"

require("core")
